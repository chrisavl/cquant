# CQuant
A simple tool for image color quantization.

Build/run with sbt (_N_ is the number of colors wanted):
    $ sbt
    > run /path/to/image N
