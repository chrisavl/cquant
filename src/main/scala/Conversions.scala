/**
 * Copyright 2012 (C) Christian Lundgren (MIT LICENSE)
 */

package cquant

import java.awt.image.BufferedImage

/**
 * Some implicit conversions to make life a bit easier ;)
 */
object Conversions {
  implicit def imageToList(image: BufferedImage): List[Pixel] = {
    val ret = for (x <- 0 until image.getWidth; y <- 0 until image.getHeight)
      yield intToPixel(image.getRGB(x, y), x, y)
    ret.toList
  }
  def intToPixel(argb :Int, x: Int = 0, y: Int = 0): Pixel = {
    val r = (argb & 0x00ff0000) >> 16
    val g = (argb & 0x0000ff00) >> 8
    val b = (argb & 0x000000ff)
    Pixel(r, g, b, x, y)
  }
  implicit def listToPixel(rgb: List[Int]): Pixel = {
    assert(rgb.size == 3, "Trying to convert invalid list to a color!")
    Pixel(rgb(0), rgb(1), rgb(2), -1, -1)
  }
}

