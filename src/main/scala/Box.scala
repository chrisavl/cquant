/**
 * Copyright 2012 (C) Christian Lundgren (MIT LICENSE)
 */

package cquant

import Dimension._

/**
 * Represents a clustering of Pixels.
 */
class Box(val pixels: List[Pixel], val upperBound: Pixel, val lowerBound: Pixel) {
  def this(pixels: List[Pixel]) = this(pixels, null, null)

  /**
   * Returns a new minimum Box spanning the contained pixels.
   */
  def shrink = {
    assert(!pixels.isEmpty, "Cannot shrink empty Box!")
    
    /* Find the max/min in each dimension of color space */
    val newMax = Dimension.values.toList.map({d => pixels.map(_.get(d)).max})
    val newMin = Dimension.values.toList.map({d => pixels.map(_.get(d)).min})

    import Conversions._
    
    new Box(pixels, newMax, newMin)
  }

  /**
   * Returns the the longest Dimension of this Box.
   */
  def maxDimension: Dimension =
    Dimension.values.map(length _).zip(Dimension.values).max._2

  /**
   * Returns the length of the longest Dimension of this Box.
   */
  def maxDimensionLength = Dimension.values.map(length _).max

  /**
   * Returns the length of a particular Dimension.
   */
  def length(d: Dimension): Int = upperBound.get(d) - lowerBound.get(d)

  /**
   * Returns the mean color point of this Box.
   */
  def mean: Pixel = pixels.reduceLeft(_ + _) / pixels.length

  /**
   * Returns a tuple of two List[Pixel] which is the result of splitting
   * the Box' pixels at the median along Dimension @d.
   */
  def split(d: Dimension) = {
    val range = pixels.map(_.get(d))
    val nth = range.size / 2 + 1
    val median = Algorithm.findNth(range, nth)
    val (low, tmp) = pixels.partition(_.get(d) < median)
    val (medians, high) = tmp.partition(_.get(d) == median)
    val mid = nth - low.length
    (low ++ medians.take(mid), medians.takeRight(medians.length - mid) ++ high)
  }
}

