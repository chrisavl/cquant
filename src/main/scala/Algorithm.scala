/**
 * Copyright 2012 (C) Christian Lundgren (MIT LICENSE)
 */

package cquant

import collection.mutable.PriorityQueue
import util.Random

object Algorithm 
{
  /**
   * Returns a list of tuples of length @paletteSize which contains the
   * clustering of the pixels in color space and their associated mean.
   */
  def medianCut(image: List[Pixel], paletteSize: Short) = {
    val boxes = new PriorityQueue[Box]()(Ordering.by(_.maxDimensionLength))
    val initialBox = new Box(image).shrink
    boxes.enqueue(initialBox)

    while (boxes.size < paletteSize) {
      /* Dequeue the largest Box */
      val largestBox = boxes.dequeue()
      val d = largestBox.maxDimension
      
      /* Split the Box at the median along it's largest Dimension */
      val parts = largestBox.split(d)

      /* Shrink and enqueue the partitions */
      val leftBox = new Box(parts._1).shrink
      boxes.enqueue(leftBox)

      val rightBox = new Box(parts._2).shrink
      boxes.enqueue(rightBox)
    }

    boxes.map { b => b.mean } toList
  }

  /**
   * Helper function for finding the nth ordered value quickly, why doesn't
   * Scala have something like std::nth_element??
   */
  def findNth[A <% Ordered[A]](range: List[A], nth: Int): A = {
    val rand = new Random(System.currentTimeMillis());
    val pivot = range(rand.nextInt(range.length))
    val low = range.filter(_ < pivot)
    val high = range.filter(_ > pivot)
    val i = low.length
    val j = range.length - high.length
    if (nth < i)
      findNth(low, nth)
    else if (nth >= j)
      findNth(high, nth - j)
    else
      pivot
  }
}

