/**
 * Copyright 2012 (C) Christian Lundgren (MIT LICENSE)
 *
 * A simple tool for color quantization.
 * 
 * Compile with:
 * scalac Main.scala
 *
 * Run with (N is the number of colors for the quantizised image):
 * scala org.chrisavl.cquant.Main /path/to/image.jpg N
 *
 * Produces the quantizised image "out.png" in the current folder.
 */

package cquant

import java.io.File
import javax.imageio.ImageIO

object Main extends App {
  def help {
    println("Please provide the arguments: Filename NumberOfColors")
    println("Description: Quantizes the input image and produces the results in out.png")
    System.exit(1)
  }
  
  override def main(args: Array[String]) = {
    if (args.size != 2)
      help

    val image = ImageIO.read(new File(args(0)))
    val numColors = args(1).toShort

    import Conversions._
    
    /* Get the new palette */
    val palette = Algorithm.medianCut(image, numColors)

    /* Index the colors/pixels with the palette */
    image.foreach { p: Pixel => image.setRGB(p.x, p.y, p.toIndexed(palette)) }

    ImageIO.write(image, "png", new File("out.png"))
  }

}

