/**
 * Copyright 2012 (C) Christian Lundgren (MIT LICENSE)
 */

package cquant

/**
 * Enumeration of the dimensions of color space.
 */
object Dimension extends Enumeration {
  type Dimension = Value
  val Red = Value("red")
  val Green = Value("green")
  val Blue = Value("blue")
}

