/**
 * Copyright 2012 (C) Christian Lundgren (MIT LICENSE)
 */

package cquant

import Dimension._

/**
 * Represents a Pixel in an image or an average of Pixel colors.
 */
case class Pixel(val r: Int, val g: Int, val b: Int, val x: Int, val y: Int) {
  /**
   * Returns the value of the Pixel in the given Dimension.
   */
  def get(d: Dimension): Int = d match {
    case Red => r
    case Green => g
    case Blue => b
    case _ => throw new IndexOutOfBoundsException()
  }

  import math.pow
  /**
   * Returns the Euclidean distance between two pixels in RGB space.
   */
  def distance(p: Pixel) = math.sqrt(pow(r-p.r, 2) + pow(g-p.g, 2) + pow(b-p.b, 2))

  /* Helper functions for calculating average colors */
  def +(rhs: Pixel) = Pixel(r + rhs.r, g + rhs.g, b + rhs.b, -1, -1)
  def /(rhs: Int) = Pixel(r / rhs, g / rhs, b / rhs, -1, -1)

  /**
   * Returns the color represented by ImageIO's ARGB format.
   */
  def toInt: Int = 0xff000000| /* Set alpha level to opaque */
    ((r << 16) & 0x00ff0000) |
    ((g << 8) & 0x0000ff00)  |
    (b & 0x000000ff)

  /**
   * Returns the best representation of the Pixel given a palette.
   */
  def toIndexed(palette: List[Pixel]) = palette.minBy { distance } toInt
}

